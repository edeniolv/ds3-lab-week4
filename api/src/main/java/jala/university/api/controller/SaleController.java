package jala.university.api.controller;

import jala.university.api.model.entity.Sale;
import jala.university.api.model.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/sales")
public class SaleController {
    private final SaleService service;

    @Autowired
    public SaleController(SaleService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<Sale> createSale(@RequestBody Sale body) {
        Sale sale = service.create(body);

        if (sale == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(sale, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Iterable<Sale>> readAllSales() {
        Iterable<Sale> sales = service.readAll();

        if (sales == null) {
            return new ResponseEntity<>(HttpStatus.OK);
        }

        return new ResponseEntity<>(sales, HttpStatus.OK);
    }
}
