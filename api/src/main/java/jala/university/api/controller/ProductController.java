package jala.university.api.controller;

import jala.university.api.model.entity.Product;
import jala.university.api.model.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/api/products")
public class ProductController {
    private final ProductService service;

    @Autowired
    public ProductController(ProductService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<Product> createProduct(
        @RequestBody Product body
    ) {
        Product product = service.create(body);

        if (product == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(product, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Iterable<Product>> readAllProducts() {
        Iterable<Product> products = service.readAll();

        if (products == null) {
            return new ResponseEntity<>(HttpStatus.OK);
        }

        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> readProductById(@PathVariable UUID id) {
        Product product = service.readById(id);

        if (product == null) {
            return new ResponseEntity<>(HttpStatus.OK);
        }

        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @PatchMapping("/{id}/name")
    public ResponseEntity<Product> updateProductName(
        @PathVariable UUID id,
        @RequestBody Map<String, String> body
    ) {
        Product product = service.updateName(id, body.get("name"));

        if (product == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @PatchMapping("/{id}/price")
    public ResponseEntity<Product> updateProductPrice(
        @PathVariable UUID id,
        @RequestBody Map<String, Double> body
    ) {
        Product product = service.updatePrice(id, body.get("price"));

        if (product == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @PatchMapping("/{id}/quantity/add")
    public ResponseEntity<Product> updateProductAddQuantity(
        @PathVariable UUID id,
        @RequestBody Map<String, Integer> body
    ) {
        Product product = service.updateAddQuantity(id, body.get("quantity"));

        if (product == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @PatchMapping("/{id}/quantity/subtract")
    public ResponseEntity<Product> updateProductSubtractQuantity(
        @PathVariable UUID id,
        @RequestBody Map<String, Integer> body
    ) {
        Product product = service.updateRemoveQuantity(id, body.get("quantity"));

        if (product == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteProduct(@PathVariable UUID id) {
        if (service.delete(id) == 0) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
