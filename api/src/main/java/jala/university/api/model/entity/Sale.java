package jala.university.api.model.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Data
@Table(name = "sales")
public class Sale {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id", nullable = false)
    private UUID id;

    @OneToOne
    @JoinColumn(name = "product_fk")
    private Product product;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "date")
    private LocalDateTime date;

    @Column(name = "total_value")
    private double totalValue;

    public Sale() {
        date = LocalDateTime.now();
    }
}
