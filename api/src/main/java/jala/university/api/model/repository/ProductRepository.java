package jala.university.api.model.repository;

import jakarta.transaction.Transactional;
import jala.university.api.model.entity.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface ProductRepository extends CrudRepository<Product, UUID> {
    Product findProductByIdEquals(UUID id);
    Product findProductByNameEquals(String name);
    @Transactional
    Integer deleteByIdEquals(UUID id);
}
