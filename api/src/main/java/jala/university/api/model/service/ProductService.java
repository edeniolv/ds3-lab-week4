package jala.university.api.model.service;

import jala.university.api.model.entity.Product;
import jala.university.api.model.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ProductService {
    private final ProductRepository repository;

    @Autowired
    public ProductService(ProductRepository repository) {
        this.repository = repository;
    }

    public Product create(Product product) {
        if (product == null) {
            return null;
        }

        if (!isValidProduct(product)) {
            return null;
        }

        return repository.save(product);
    }

    public Iterable<Product> readAll() {
        return repository.findAll();
    }

    public Product readById(UUID id) {
        return repository.findProductByIdEquals(id);
    }

    public Product readByName(String name) {
        return repository.findProductByNameEquals(name);
    }

    public Product updateAddQuantity(UUID id, int value) {
        Product product = repository.findProductByIdEquals(id);

        if (product == null) {
            return null;
        }

        product.setQuantity(product.getQuantity() + value);
        return repository.save(product);
    }

    public Product updateRemoveQuantity(UUID id, int value) {
        Product product = repository.findProductByIdEquals(id);

        if (product == null) {
            return null;
        }

        product.setQuantity(product.getQuantity() - value);
        return repository.save(product);
    }

    public Product updatePrice(UUID id, double value) {
        Product product = repository.findProductByIdEquals(id);

        if (product == null) {
            return null;
        }

        product.setPrice(value);
        return repository.save(product);
    }

    public Product updateName(UUID id, String name) {
        Product product = repository.findProductByIdEquals(id);

        if (product == null) {
            return null;
        }

        product.setName(name);
        return repository.save(product);
    }

    public Integer delete(UUID id) {
        return repository.deleteByIdEquals(id);
    }

    private boolean isValidProduct(Product product) {
        return
            !product.getName().isEmpty() &&
            product.getPrice() > 0 &&
            product.getQuantity() >= 0;
    }
}
