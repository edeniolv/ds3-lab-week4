package jala.university.api.model.repository;

import jala.university.api.model.entity.Sale;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface SaleRepository extends CrudRepository<Sale, UUID> { }
