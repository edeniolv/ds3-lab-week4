package jala.university.api.model.service;

import jala.university.api.model.entity.Product;
import jala.university.api.model.entity.Sale;
import jala.university.api.model.repository.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class SaleService {
    private final SaleRepository repository;
    private final ProductService productService;

    @Autowired
    public SaleService(
        SaleRepository repository,
        ProductService productService
    ) {
        this.repository = repository;
        this.productService = productService;
    }

    public Sale create(Sale sale) {
        Product product = getProduct(sale.getProduct().getName());

        if (product == null) {
            return null;
        }

        sale.setProduct(product);

        if (!isValidSale(sale)) {
            return null;
        }

        sale.setTotalValue(getTotalValue(sale));
        return repository.save(sale);
    }

    public Iterable<Sale> readAll() {
        return repository.findAll();
    }

    private boolean isValidSale(Sale sale) {
        if (sale.getQuantity() > sale.getProduct().getQuantity()) {
            return false;
        }

        if (sale.getProduct().getPrice() <= 0) {
            return false;
        }

        if (sale.getQuantity() < 0) {
            return false;
        }

        return updateProductQuantity(
            sale.getProduct().getId(), sale.getQuantity()
        );
    }

    private Product getProduct(String name) {
        return productService.readByName(name);
    }

    private boolean updateProductQuantity(UUID id, int quantity) {
        return productService.updateRemoveQuantity(id, quantity) != null;
    }

    private double getTotalValue(Sale sale) {
        double total = sale.getProduct().getPrice() * sale.getQuantity();

        if (sale.getQuantity() > 20) {
            return applyDiscount(total, 10);
        }

        if (sale.getQuantity() > 10) {
            return applyDiscount(total, 5);
        }

        return total;
    }

    private double applyDiscount(double total, int discount) {
        return total - (total * discount / 100);
    }
}
